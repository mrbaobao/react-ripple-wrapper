# react-ripple-wrapper

React Ripple effect
# Install:
* yarn add react-ripple-wrapper
* npm i react-ripple-wrapper

# Usage:
```
import * as Ripple from "react-ripple-wrapper"
...
<Ripple time={500} className="the-ripple">
    <div>
        Content here
    </div>
</Ripple>
```