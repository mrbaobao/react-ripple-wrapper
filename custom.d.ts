import 'react';

declare module 'react' {
    interface StyleHTMLAttributes<T> extends React.HTMLAttributes<T> {
        jsx?: boolean;
        global?: boolean;
    }
    interface HTMLAttributes<T> extends React.DOMAttributes<T> {
        attribution?: string;
        page_id?: string;
        logged_in_greeting?: string;
        logged_out_greeting?: string;
        ariaValuemax?: number;
        ariaValuemin?: number;
        ariaValuenow?: number;
    }
}
