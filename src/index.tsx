
import * as React from "react"
interface RippleProps {
    time?: number //ms
    className?: string
}
interface RippleState {
    x: number,
    y: number,
    r: number,
    show: boolean
}
const defaultTime = 500 //ms
class Ripple extends React.Component<RippleProps, RippleState>{
    //Circle
    circle: SVGCircleElement
    wrapper: HTMLDivElement
    constructor(props: any) {
        super(props)
        this.state = {
            x: 0,
            y: 0,
            r: 0,
            show: false
        }
    }
    //Reset
    reset = () => {
        this.circle.setAttribute("r", "0")
        this.setState({
            x: 0,
            y: 0,
            r: 0,
            show: false
        })
    }
    wrapperSize = {
        left: 0,
        top: 0,
        width: 0,
        height: 0
    }
    /**
     * Store sizes
     */
    componentDidMount() {
        this.wrapperSize.left = this.wrapper.offsetLeft
        this.wrapperSize.top = this.wrapper.offsetTop
        this.wrapperSize.width = this.wrapper.clientWidth
        this.wrapperSize.height = this.wrapper.clientHeight
    }
    //rippleEffect
    rippleEffect = (e) => {
        const x = e.clientX - this.wrapperSize.left
        const y = e.clientY - this.wrapperSize.top
        const w = this.wrapperSize.width
        const h = this.wrapperSize.height
        const maxR = Math.max(
            Math.sqrt(x * x + y * y),
            Math.sqrt((w - x) * (w - x) + (h - y) * (h - y)),
            Math.sqrt((w - x) * (w - x) + y * y),
            Math.sqrt(x * x + (h - y) * (h - y)),
        )
        let r = 0
        //Init
        this.setState({
            x,
            y,
            r,
            show: true
        })
        const timer = setInterval(() => {
            if (this.circle.classList.contains("show")) {
                this.circle.setAttribute("r", maxR + "")
                clearInterval(timer)
            }
        })
        //Reset
        setTimeout(() => {
            this.reset()
        }, this.props.time || defaultTime)
    }
    //Render
    render() {
        const { x, y, r, show } = this.state
        const { className } = this.props
        return (
            <div className={"ripple-wrapper " + (className ? className : "") + (show ? " ripple " : "")} onClick={this.rippleEffect} ref={w => this.wrapper = w}>
                <style jsx>{`
                    .ripple-wrapper{
                        position: relative;
                    }
                    svg {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 0;
                        height: 0;
                    }
                    svg.show{
                        width: 100%;
                        height: 100%;
                    }
                    circle {
                        fill: rgba(255,255,255,0.1);
                        transform: translated3d(0, 0, 0);
                    }
                `}</style>
                {this.props.children}
                <svg className={show ? "show" : ""}>
                    <circle style={{ transition: "r ease-in " + (this.props.time || defaultTime) * 0.001 + "s" }} ref={c => this.circle = c}
                        className={show ? "show" : ""}
                        cx={x} cy={y} r={r} ></circle>
                </svg>
            </div>
        )
    }
}
//Export
export default Ripple